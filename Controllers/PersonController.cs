using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using swaggerpoc.Models;

namespace swaggerpoc.Controllers {
  [Route ("api/[controller]")]
  public class PersonController : Controller {
    [HttpGet ("")]
    public IEnumerable<Person> GetPeople () {
      List<Person> people = new List<Person> ();

      for (int i = 0; i < 5; i++) {
        people.Add (new Person {
          Id = i + 1,
            Name = $"Person {i + 1}",
            Gender = (Gender) (i % 2)
        });
      }

      return people;
    }

    [HttpGet ("{id}")]
    public Person GetPerson (int id) {
      var person = new Person {
        Id = id,
        Name = $"Person {id}",
        Gender = (Gender) (id % 2)
      };

      return person;
    }
  }
}