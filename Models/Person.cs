using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace swaggerpoc.Models {
  public enum Gender {
    Male,
    Female
  }

  public class Person {
    [Required]
    public int Id { get; set; }
    
    public string Name { get; set; }

    [JsonConverter (typeof (StringEnumConverter))]
    public Gender Gender { get; set; }
  }
}